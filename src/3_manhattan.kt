import kotlin.math.abs
import kotlin.math.max
import kotlin.math.sign

// Primera parte
fun frameEndFor(n: Int): Int {
    var i = 3
    while (i * i < n) {
        i += 2
    }
    return i
}

fun posInGroup(frameEndIx: Int, n: Int): Int {
    val frameStartVal = (frameEndIx - 2) * (frameEndIx - 2) + 1
    return (n - frameStartVal) % (frameEndIx - 1)
}

fun diffInSub(ix: Int, len: Int): Int {
    if (ix == len - 1)
        return 0
    val newLen = len - 1
    val middle = newLen / 2
    return abs(middle - ix) - middle - 1
}

fun distance(n: Int): Int {
    val fe = frameEndFor(n)
    val pig = posInGroup(fe, n)
    val dis = diffInSub(pig, fe - 1)
    return fe + dis - 1
}

operator fun Pair<Int, Int>.plus (other: Pair<Int, Int>): Pair<Int, Int> =
        Pair(this.first + other.first, this.second + other.second)

fun Pair<Int, Int>.getFrame(): Int = max(abs(this.first), abs(this.second))

// Segunda parte
class Spiral(until: Int) {
    private val spiral = mutableMapOf(Pair(0, 0) to 1)
    var lastPos = Pair(0, 0)
        set(newPos) {
            spiral.put(newPos, calculateValueFor(newPos))
            field = newPos
            if (newPos.first == -newPos.second && sign(newPos.first.toDouble()) == 1.0) {
                squareSize += 1
            }
        }
    private var squareSize = 1

    fun nextPosition(): Pair<Int, Int> {
//        print(lastPos)
        if (lastPos.first == -lastPos.second && sign(lastPos.first.toDouble()) == 1.0 && squareSize == -lastPos.first) {
            // We need to make the square bigger
//            println(" -> bigger: ${lastPos + Pair(1, 0)}")
            return lastPos + Pair(1, 0)
        }
        for (movement in arrayListOf(Pair(1, 0), Pair(0, 1), Pair(-1, 0), Pair(0, -1))) {
            val tentative = lastPos + movement
            if (tentative.getFrame() == squareSize && spiral[tentative] == null) {
//                println(" -> accept: $tentative")
                return tentative
            }
//            print(" -> reject: $tentative")
        }
//        println(" -> ERROR")
        throw Exception("WTF")
    }

    private fun calculateValueFor(pos: Pair<Int, Int>): Int {
        var sum = 0
        for (x in -1..1) {
            for (y in -1..1) {
                sum += spiral[pos + Pair(x, y)] ?: 0
            }
        }
        return sum
    }

    fun valueOf(pos: Pair<Int, Int>): Int = spiral[pos] ?: -1
}

fun main(args: Array<String>) {
    val input = 361527
    // println(distance(input))

    // 2da
    val spiral = Spiral(0)
    var prevValue = 1
    while (spiral.valueOf(spiral.lastPos) < input) {
        val newPos = spiral.nextPosition()
        spiral.lastPos = newPos  // bad interface design
        prevValue = spiral.valueOf(spiral.lastPos)
    }
    println(prevValue)
}

