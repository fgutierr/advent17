import java.io.File

fun String.isAnagramOf(other: String): Boolean {
    return this.toCharArray().sorted().toString() == other.toCharArray().sorted().toString()
}

fun List<String>.hasAnagrams(): Boolean {
    var ix = 0
    for (s1 in this.subList(ix, this.size - 1)) {
        for (s2 in this.subList(ix + 1, this.size)) {
            if (s1.isAnagramOf(s2)) {
                return true
            }
        }
        ix += 1
    }
    return false
}

fun main(args: Array<String>) {
    var count = 0
    File("input-files/4").useLines { lines -> lines.forEach {
        val splitted = it.split(" ")
        count += if (splitted.distinct().size == splitted.size && !splitted.hasAnagrams()) 1 else 0
    } }
    println(count)

    println("oiii ioii iioi iiio".split(" ").hasAnagrams())
    println("iiii oiii ooii oooi oooo".split(" ").hasAnagrams())
}