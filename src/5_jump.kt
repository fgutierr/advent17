import java.io.File

fun main(args: Array<String>) {
    val ins = mutableListOf<Int>()
    File("input-files/5").useLines { lines -> lines.forEach { ins.add(it.toInt()) } }
    var ix = ins[0]
    var nJumps = 0
    while (ix < ins.size) {
        val newIx = ins[ix] + ix
        ins[ix] += if (ins[ix] < 3) 1 else -1
        ix = newIx
        nJumps++
    }
    println(nJumps)
}