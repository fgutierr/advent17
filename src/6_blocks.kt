
fun hash(xs: List<Int>): String = xs.joinToString(",") { it.toString() }

fun main(args: Array<String>) {
    //val bank = "0\t2\t3"
    val bank = "4\t10\t4\t1\t8\t4\t9\t14\t5\t1\t14\t15\t0\t15\t3\t5"
            .split("\t")
            .map { it.toInt() }
            .toMutableList()

    val cache = mutableMapOf<String, Boolean>()
    var counter = 0

    while (!cache.getOrDefault(hash(bank), false)) {
        cache.put(hash(bank), true)
        var ix = bank.indexOf(bank.max())
        var rem = bank[ix]
        bank[ix] = 0
        while (rem > 0) {
            bank[(ix + 1) % bank.size] += 1
            rem--
            ix++
        }
        //println(bank)
        counter++
    }

    println(counter)
}